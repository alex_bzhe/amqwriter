package org.amq.amqwriter;

import lombok.RequiredArgsConstructor;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;

@Configuration
@EnableJms
@RequiredArgsConstructor
@EnableConfigurationProperties(ActiveMqProperties.class)
public class ActiveMqConfiguration {

    private final ActiveMqProperties activeMqProperties;

    @Bean(name = "messageQueue")
    public Queue messageQueue() {
        return new ActiveMQQueue(activeMqProperties.getMessageQueueName());
    }

    @Bean(name = "messageTopic")
    public Topic messageTopic() {
        return new ActiveMQTopic(activeMqProperties.getMessageTopicName());
    }

    @Bean(name = "messageQueueTemplate")
    public JmsTemplate queueTemplate(){
        JmsTemplate template = new JmsTemplate(connectionFactory());
        template.setDefaultDestination(messageQueue());
        return template;
    }

    @Bean(name = "messageTopicTemplate")
    public JmsTemplate topicTemplate(){
        JmsTemplate template = new JmsTemplate(connectionFactory());
        template.setDefaultDestination(messageTopic());
        template.setPubSubDomain(true);
        return template;
    }

    @Bean(name = "connectionFactory")
    public ConnectionFactory connectionFactory(){
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(activeMqProperties.getBrokerUrl());
        connectionFactory.setPassword(activeMqProperties.getPassword());
        connectionFactory.setUserName(activeMqProperties.getUsername());
        return new CachingConnectionFactory(connectionFactory);
    }

    @Bean(name = "queueListenerContainerFactory")
    public DefaultJmsListenerContainerFactory queueListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
        return factory;
    }

    @Bean(name = "topicListenerContainerFactory")
    public DefaultJmsListenerContainerFactory topicListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setPubSubDomain(true);
        return factory;
    }
}