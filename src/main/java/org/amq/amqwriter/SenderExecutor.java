package org.amq.amqwriter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.config.JmsListenerEndpointRegistry;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@RequiredArgsConstructor
public class SenderExecutor implements CommandLineRunner {

    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private final ApplicationContext appContext;
    private final JmsListenerEndpointRegistry jmsListenerEndpointRegistry;
    private final Sender sender;

    public void run(String... arg0) {
        log.trace("AMQWriter: started");
        executorService.scheduleAtFixedRate(sender, 5, 10, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
        jmsListenerEndpointRegistry.stop();
        log.trace("AMQWriter: finished");
        SpringApplication.exit(appContext, () -> 0);
    }
}