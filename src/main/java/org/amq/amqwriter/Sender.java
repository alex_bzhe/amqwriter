package org.amq.amqwriter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Component
@RequiredArgsConstructor
public class Sender implements Runnable {

    private static final String PATTERN = "yyyy-MM-dd HH:mm:ss Z";
    private final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(PATTERN);

    @Qualifier("messageQueueTemplate")
    private final JmsTemplate messageQueueTemplate;

    @Qualifier("messageTopicTemplate")
    private final JmsTemplate messageTopicTemplate;

    @Override
    public void run() {
        final String message = SIMPLE_DATE_FORMAT.format(new Date());
        messageQueueTemplate.convertAndSend(message);
        messageTopicTemplate.convertAndSend(message);
        log.trace("sent to message topic and queue: " + message);
    }
}
