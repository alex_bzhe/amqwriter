package org.amq.amqwriter;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "spring.activemq")
public class ActiveMqProperties {

    private String brokerUrl;
    private String username;
    private String password;
    private String messageQueueName;
    private String messageTopicName;
}
