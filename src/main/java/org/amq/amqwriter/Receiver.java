package org.amq.amqwriter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Slf4j
@Component
@RequiredArgsConstructor
public class Receiver {

    private final SenderExecutor senderExecutor;

    @JmsListener(
            destination = "${spring.activemq.command-queue-name}",
            containerFactory = "queueListenerContainerFactory")
    public void receiveQueue(Message message) throws JMSException {
        String text = ((TextMessage) message).getText();
        message.acknowledge();
        log.trace("got from command queue: " + text);
        if (text.contains("stop date sender")) {
            senderExecutor.stop();
        }
    }

    @JmsListener(
            destination = "${spring.activemq.command-topic-name}",
            containerFactory = "topicListenerContainerFactory")
    public void receiveTopic(String text) {
        log.trace("got from command topic: " + text);
        if (text.contains("stop all") || text.contains("stop date sender")) {
            senderExecutor.stop();
        }
    }
}