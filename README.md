
### About ###
This is a test standalone Spring Boot app which consumes and produces messages using ActiveMQ.

### Details ###
1. App uses default settings (see application.properties) to connect to ActiveMQ which should be provided.
2. On startup app begins to periodically send messages with current time to 'message' queue and to 'message' topic.
3. Also app receives messages from 'command' queue and from 'command' topic.
4. Sent and received messages are logged to console.
5. When received message contains text "stop date sender" or message from 'command' topic contains text "stop all" the app shutdowns. 
6. The shutdown starts after the received message is dequeued from the ActiveMQ.